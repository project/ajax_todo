<?php

function ajax_todo_set_headers()
{
    // don't allow browsers to cache the contents of this page because it may change
    header('Cache-Control: no-cache, must-revalidate');
    header('Pragma: no-cache'); // for HTTP/1.0
    header('Expires: Thu, 01 Jan 1970 00:00:00 GMT');
}

// Init the list with all current notes
function ajax_todo_init()
{
    ajax_todo_set_headers();
    header('Content-type: text/javascript');

    $output = '';

    // first do a cleanup of the notes list (remove notes which are added but not edited)
    db_query('DELETE FROM ajax_todo_task WHERE text = ""');

    $result = db_query('SELECT id, text, complete_date FROM ajax_todo_task WHERE archived != 1');

    while ($row = db_fetch_array($result)) {
        $text = addslashes($row['text']);

        // TODO: return JSON
        // we are returning javascript code which will call the addnote function
        $output .= "addnote({$row['id']}, '$text', '".$row['complete_date']."'); \n ";
    }

    die($output);
}

// Request for a new id
function ajax_todo_new()
{
    ajax_todo_set_headers();
    $output = '';

    // insert a new empty note and return it's id
    if (db_query('INSERT INTO ajax_todo_task (text) VALUES("")')) {
        $output = db_last_insert_id('task', 'id');
    }
    else {
        // TODO: add more error handling
        $output = '0';
    }

    die($output);
}

// Delete a note
function ajax_todo_del($id)
{
    ajax_todo_set_headers();

    if (is_numeric($id) && $id >= 0)
    {
        db_query('UPDATE ajax_todo_task SET archived = 1 WHERE id = %d', $id);
        db_query('DELETE FROM ajax_todo_task WHERE (id = %d) AND (complete_date = "0000-00-00")', $id);
    }

    die('');
}

// Update the contents of a note
function ajax_todo_save()
{
    ajax_todo_set_headers();

    if (isset($_POST['id']) && is_numeric($_POST['id'])) {
        if (isset($_POST['compdate'])) {
            // strip the tags from the note text
            $text = strip_tags($_POST['text']);
            $compdate = $_POST['compdate'];

            db_query("UPDATE ajax_todo_task SET text = '%s', complete_date = '%s' WHERE id = %d", $text, $compdate, $_POST['id']);
        }
        else {
            // strip the tags from the note text
            $text = strip_tags($_POST['text']);

            db_query("UPDATE ajax_todo_task SET text = '%s' WHERE id = %d", $text, $_POST['id']);
        }
    }

    die ('');
}
?>
