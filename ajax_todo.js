/**************************************************************************
  *  The beginnings of this script was found at http://www.codepost.org/view.php?id=120
  *  Posted by Erik on Feb 10 2006 @ 13:41
  *
  *  Name: Ajax todo list
  *
  *  Edited on Nov. 29, 2007 by Chris Hess (http://cjhess.com)
  *  Ported to Drupal by Yonas Yanfa (yonas.yanfa@kudoscoins.com)
  **************************************************************************/

/*
    // the list element
    var list = document.getElementById('list');

    // seperator between the actual list and the add link
    //var sep  = list.appendChild(document.createElement('br'));

    // the add link
    //var add           = list.appendChild(document.createElement('li'));
    //    add.innerHTML = '<br /><a href="#" onclick="return false" style="text-decoration:none;"><button>Add an Item</button></a>';
    //    add.value     = '';

*/

// from: http://www.codepost.org/view/59
function createXMLHttpRequest() {
  try {
    return new XMLHttpRequest();
  } catch(e) { }

  var types = [
    'Microsoft.XMLHTTP',
    'MSXML2.XMLHTTP.5.0',
    'MSXML2.XMLHTTP.4.0',
    'MSXML2.XMLHTTP.3.0',
    'MSXML2.XMLHTTP'
   ];

  for (var i = 0; i < types.length; i++) {
    try {
      return new ActiveXObject(types[i]);
    } catch(e) {}
  }

  return false; // XMLHttpRequest not supported
}


// This function will be called when the add link is pressed
// and when loading the list at startup
function addnote(id, text, completedate) {
  var image_path = '/sites/all/modules/ajax_todo/images/';
  //var item = list.insertBefore(document.createElement('li'), sep);

  // the list element
  var list = document.getElementById('list');
  var item = list.appendChild(document.createElement('li'));
	  item.className = id;
	  
  // inputs for checking (Had to use images because IE was having a problem with the input type 'checked' - or anything but 'text' for that matter...
 	   
 var checked          = item.appendChild(document.createElement('img'));
      checked.src     = image_path + 'checked.gif';
	  checked.style.display = 'none';
	  
  var unchecked          = item.appendChild(document.createElement('img'));
      unchecked.src     = image_path + 'unchecked.gif';
	  unchecked.style.display = 'inline';
  
  var compdate = item.appendChild(document.createElement('input'));
	  compdate.type = 'text';
	  compdate.style.display = 'none';
	  compdate.value = completedate;
	  compdate.maxLength = 10;
	  
  // span containing the html
  var html = item.appendChild(document.createElement('span'));
  
  // input for editing
  var edit           = item.appendChild(document.createElement('input'));
      edit.type      = 'text';
      edit.value     = text;
      edit.maxLength = 100;
      edit.size      = 80;
	  edit.style.display = 'none';
	  
 
  // image for the delete button
  var dele               = item.appendChild(document.createElement('img'));
      dele.src           = image_path + 'dele.gif';
      dele.style.display = 'none';

  if(compdate.value != '0000-00-00 00:00:00')
  {
	  unchecked.style.display = 'none';
	  checked.style.display = 'inline';
	  checked.value = 1;
  }

  // new note?
  if (id == -1) {
    // use an xmlhttprequest to get a new id for the note
    var req = createXMLHttpRequest();
    req.onreadystatechange = function() {
      if (req.readyState == 4) {
        if (req.status == 200) {
          item.id = req.responseText;
        }
      }
    };

    req.open('GET', '/ajax_todo_new', true);
    req.send('');
  } else {
    item.id = id;
  }

	checked.onmousedown = function() 
	{
		var t= edit.value;
		
		t = t.replace('<del>', '');   // remove <del>
		t = t.replace('</del>', ''); // remove </del>
		edit.value = t;
		
		checked.value = 0;
		compdate.value = '0000-00-00 00:00:00';
		
		var req = createXMLHttpRequest();
          req.open('POST', '/ajax_todo_save', true);
          req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
          req.send('id=' + item.id + '&text=' + escape(edit.value) + '&compdate=' + compdate.value);
			
		unchecked.style.display = 'inline';
		checked.style.display = 'none';
	
		html.onclick();
		edit.blur();
	};
	
	unchecked.onmouseover = function()
	{
		unchecked.src = image_path + 'unchecked_hover.gif';
	}
	
	unchecked.onmouseout = function()
	{
		unchecked.src = image_path + 'unchecked.gif';
	}
	
	checked.onmouseover = function()
	{
		checked.src = image_path + 'checked_hover.gif';
	}
	
	checked.onmouseout = function()
	{
		checked.src = image_path + 'checked.gif';
	}
	
	unchecked.onmousedown = function()
	{
		var t= edit.value;
		edit.value = '<del>'+t+'</del>';
		
        // Get today's Date
        var today = document.getElementById('today');

		compdate.value = today.value;
				
		var req = createXMLHttpRequest();
          req.open('POST', '/ajax_todo_save', true);
          req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
          req.send('id=' + item.id + '&text=' + escape(edit.value) + '&compdate=' + compdate.value);
	
		checked.value = 1;
	
		unchecked.style.display = 'none';
		checked.style.display = 'inline';
	
		html.onclick();
		edit.blur();
	};

  
  html.onclick = function() {
    
	var t = edit.value;
	t = t.replace('<del>', '');   // remove <del>
	t = t.replace('</del>', ''); // remove </del>
	edit.value = t;	
	
	// switch the note to edit mode
    html.style.display = 'none';
    dele.style.display = 'inline';
	edit.style.display = 'inline';
	
    edit.focus();
	if((edit.value == 'Click [i]here[/i] to edit') || (edit.value == 'Click [b]here[/b] to edit'))
	{
		edit.select();
	}
  };

  edit.onblur = function() {
    
	var t = edit.value;
	
	if (checked.value){
	t = '<del>'+t+'</del>';
	}

		t = t.replace(/\[i](.+)\[\/i]/g, '<i>$1</i>');   // replace !...! by italic text
        t = t.replace(/\[b](.+)\[\/b]/g, '<b>$1</b>'); // replace *..* by bold text

    // has the contents of the note changed?
    if (html.innerHTML != t) {
      // use an xmlhttprequest to update the note contents in the database
      var req = createXMLHttpRequest();
          req.open('POST', '/ajax_todo_save', true);
          req.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
          req.send('id=' + item.id + '&text=' + escape(edit.value));
   }

   html.innerHTML = t;
 
   // switch the note to display mode
   html.style.display = 'inline';
   if(checked.value){
	dele.style.display = 'inline';
   }
   else {
    dele.style.display = 'none';
   }
   edit.style.display = 'none';
   
  }

  // catch the enter key to finish editing the note
  edit.onkeydown = function(e) {
    var key = 0;
    if (window.event) {
      key = window.event.keyCode;
    } else if (e) {
      key = e.keyCode; // e.which
    }

    if (key == 13) { // 13 is the enter key
      edit.onblur();
    }
  }


  dele.onmousedown = function() {

    // ask the user if he/she really wants to delete the note
    if (confirm('Are you sure you want to delete?')) {
      // use an xmlhttprequest to remove the note from the database
      var req = createXMLHttpRequest();
          req.open('GET', '/ajax_todo_del/' + item.id, true);
          req.send('');

      // the list element
      var list = document.getElementById('list');

      list.removeChild(item);
    }
  }

  // trigger onblur to switch the note to display mode and update it's innerHTML
  edit.onblur();
  
}

var i_times = 0;
//var button = '';

function add_a_note() {
  // -1 indicates a new note
  i_times++;
  if(i_times == 1){
      addnote(-1, 'Click [i]here[/i] to edit','0000-00-00 00:00:00');
  }
  else{
      addnote(-1, 'Click [b]here[/b] to edit','0000-00-00 00:00:00');
      i_times = 0;
  }
}
